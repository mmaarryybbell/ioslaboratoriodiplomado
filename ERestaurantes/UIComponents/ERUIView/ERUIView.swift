//
//  ERUIView.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/02/21.
//

import UIKit

@IBDesignable class ERUIView: UIView, ERShadow, ERBorder, ERShape {
    
    //MARK: - SHADOW
    
    var shadowStyle: ERShadowStyle = ERShadowStyle() {
        didSet { self.updateShadowAppereance() }
    }
    
    @IBInspectable internal var shadowColor: UIColor {
        get { self.shadowStyle.color }
        set { self.shadowStyle.color = newValue }
    }
    
    @IBInspectable internal var shadowRadius: CGFloat {
        get { self.shadowStyle.radius }
        set { self.shadowStyle.radius = newValue }
    }
    
    @IBInspectable internal var shadowOpacity: Float {
        get { self.shadowStyle.opacity }
        set { self.shadowStyle.opacity = newValue }
    }
    
    @IBInspectable internal var shadowOffset: CGSize {
        get { self.shadowStyle.offset }
        set { self.shadowStyle.offset = newValue }
    }
    
    //MARK: - BORDER
    var borderStyle: ERBorderStyle = ERBorderStyle() {
        didSet { self.updateBorderAppereance() }
    }
    
    @IBInspectable internal var borderColor: UIColor {
        get { self.borderStyle.color }
        set { self.borderStyle.color = newValue }
    }
    
    @IBInspectable internal var borderWidth: CGFloat {
        get { self.borderStyle.width }
        set { self.borderStyle.width = newValue }
    }
    
    //MARK: - SHAPE
    var shapeStyle: ERShapeStyle = ERShapeStyle() {
        didSet { self.updateShapeAppereance() }
    }
    
    @IBInspectable internal var cornerRadius: CGFloat {
        get { self.shapeStyle.radius }
        set { self.shapeStyle.radius = newValue }
    }
    
    @IBInspectable internal var topLeft: Bool {
        get { self.shapeStyle.topLeft }
        set { self.shapeStyle.topLeft = newValue }
    }
    
    @IBInspectable internal var topRigth: Bool {
        get { self.shapeStyle.topRigth }
        set { self.shapeStyle.topRigth = newValue }
    }
    
    @IBInspectable internal var downLeft: Bool {
        get { self.shapeStyle.downLeft }
        set { self.shapeStyle.downLeft = newValue }
    }
    
    @IBInspectable internal var downRigth: Bool {
        get { self.shapeStyle.downRigth }
        set { self.shapeStyle.downRigth = newValue }
    }
}
