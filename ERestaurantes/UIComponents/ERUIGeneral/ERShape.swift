//
//  ERShape.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/02/21.
//

import UIKit

struct ERShapeStyle {
    
    var radius      : CGFloat
    var topLeft     : Bool
    var topRigth    : Bool
    var downLeft    : Bool
    var downRigth   : Bool

    init(radius: CGFloat = 0, topLeft: Bool = true, topRigth: Bool = true, downLeft: Bool = true, downRigth: Bool = true) {
        
        self.radius     = radius
        self.topLeft    = topLeft
        self.topRigth   = topRigth
        self.downLeft   = downLeft
        self.downRigth  = downRigth
    }
}

protocol ERShape {
    
    //ESTA PROPIEDAD LA VAMOS A USAR DESDE EL CODIGO
    var shapeStyle     : ERShapeStyle { get set }
    
    //ESTAS PROPIEDADES SOLO LAS VAMOS USAR DESDE EL STORYBOARD
    var cornerRadius    : CGFloat   { get set }
    var topLeft         : Bool      { get set }
    var topRigth        : Bool      { get set }
    var downLeft        : Bool      { get set }
    var downRigth       : Bool      { get set }
}

extension ERShape where Self: UIView {
    
    func updateShapeAppereance() {
        
        self.layer.cornerRadius = self.shapeStyle.radius
        self.layer.maskedCorners = self.activateCorners()
    }
    
    func activateCorners() -> CACornerMask {
        
        var arrayCorners = [CACornerMask]()
        
        if self.shapeStyle.topLeft      { arrayCorners.append(.layerMinXMinYCorner) }
        if self.shapeStyle.topRigth     { arrayCorners.append(.layerMaxXMinYCorner) }
        if self.shapeStyle.downLeft     { arrayCorners.append(.layerMinXMaxYCorner) }
        if self.shapeStyle.downRigth    { arrayCorners.append(.layerMaxXMaxYCorner) }
        
        return CACornerMask(arrayCorners)
    }
}

