//
//  ERShadow.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/02/21.
//

import UIKit

struct ERShadowStyle {
    
    var color   : UIColor
    var radius  : CGFloat
    var opacity : Float
    var offset  : CGSize
    
    init(color: UIColor = .clear, radius: CGFloat = 5, opacity: Float = 0.4, offset: CGSize = .zero) {
        
        self.color      = color
        self.radius     = radius
        self.opacity    = opacity
        self.offset     = offset
    }
}

protocol ERShadow {
    
    //ESTA PROPIEDAD LA VAMOS A USAR DESDE EL CODIGO
    var shadowStyle     : ERShadowStyle { get set }
    
    //ESTAS PROPIEDADES SOLO LAS VAMOS USAR DESDE EL STORYBOARD
    var shadowColor     : UIColor  { get set }
    var shadowRadius    : CGFloat  { get set }
    var shadowOpacity   : Float    { get set }
    var shadowOffset    : CGSize   { get set }
}

extension ERShadow where Self: UIView {
    
    func updateShadowAppereance() {
        
        self.layer.shadowColor      = self.shadowStyle.color.cgColor
        self.layer.shadowRadius     = self.shadowStyle.radius
        self.layer.shadowOpacity    = self.shadowStyle.opacity
        self.layer.shadowOffset     = self.shadowStyle.offset
    }
}
