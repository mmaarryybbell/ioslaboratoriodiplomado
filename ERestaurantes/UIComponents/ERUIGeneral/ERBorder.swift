//
//  ERBorder.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/02/21.
//

import UIKit

struct ERBorderStyle {
    
    var color   : UIColor
    var width   : CGFloat
    
    init(color: UIColor = .clear, width: CGFloat = 0) {
        
        self.color  = color
        self.width  = width
    }
}

protocol ERBorder {
    
    //ESTA PROPIEDAD LA VAMOS A USAR DESDE EL CODIGO
    var borderStyle     : ERBorderStyle { get set }
    
    //ESTAS PROPIEDADES SOLO LAS VAMOS USAR DESDE EL STORYBOARD
    var borderColor     : UIColor  { get set }
    var borderWidth     : CGFloat  { get set }
}

extension ERBorder where Self: UIView {
    
    func updateBorderAppereance() {
        
        self.layer.borderColor = self.borderStyle.color.cgColor
        self.layer.borderWidth = self.borderStyle.width
    }
}
