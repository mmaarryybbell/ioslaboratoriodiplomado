//
//  ERUIBorderButton.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/02/21.
//

import UIKit

@IBDesignable class ERUIBorderButton: UIButton, ERBorder {
    
    //MARK: - BORDER
    var borderStyle: ERBorderStyle = ERBorderStyle() {
        didSet { self.updateBorderAppereance() }
    }
    
    @IBInspectable internal var borderColor: UIColor {
        get { self.borderStyle.color }
        set { self.borderStyle.color = newValue }
    }
    
    @IBInspectable internal  var borderWidth: CGFloat {
        get { self.borderStyle.width }
        set { self.borderStyle.width = newValue }
    }
}
