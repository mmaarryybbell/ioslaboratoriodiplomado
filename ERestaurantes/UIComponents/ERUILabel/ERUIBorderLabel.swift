//
//  ERUIBorderLabel.swift
//  ERestaurantes
//
//  Created by user190202 on 2/26/21.
//

import UIKit

@IBDesignable class ERUIBorderLabel: UILabel, ERBorder {
    
    var borderStyle: ERBorderStyle = ERBorderStyle() {
        didSet { self.updateBorderAppereance() }
    }
    
    @IBInspectable internal var borderColor: UIColor {
        get { self.borderStyle.color }
        set { self.borderStyle.color = newValue }
    }
    
    @IBInspectable internal  var borderWidth: CGFloat {
        get { self.borderStyle.width }
        set { self.borderStyle.width = newValue }
    }
}
