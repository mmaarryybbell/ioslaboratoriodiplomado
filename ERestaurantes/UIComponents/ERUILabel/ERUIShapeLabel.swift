//
//  ERUIShapeLabel.swift
//  ERestaurantes
//
//  Created by user190202 on 2/26/21.
//
import UIKit

@IBDesignable class ERUIShapeLabel: ERUIBorderLabel, ERShape {
    
    var shapeStyle: ERShapeStyle = ERShapeStyle() {
        didSet { self.updateShapeAppereance() }
    }
    
    @IBInspectable internal var cornerRadius: CGFloat {
        get { self.shapeStyle.radius }
        set { self.shapeStyle.radius = newValue }
    }
    
    @IBInspectable internal var topLeft: Bool {
        get { self.shapeStyle.topLeft }
        set { self.shapeStyle.topLeft = newValue }
    }
    
    @IBInspectable internal var topRigth: Bool {
        get { self.shapeStyle.topRigth }
        set { self.shapeStyle.topRigth = newValue }
    }
    
    @IBInspectable internal var downLeft: Bool {
        get { self.shapeStyle.downLeft }
        set { self.shapeStyle.downLeft = newValue }
    }
    
    @IBInspectable internal var downRigth: Bool {
        get { self.shapeStyle.downRigth }
        set { self.shapeStyle.downRigth = newValue }
    }
}
