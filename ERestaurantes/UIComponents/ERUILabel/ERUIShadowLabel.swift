//
//  ERUIShadowLabel.swift
//  ERestaurantes
//
//  Created by user190202 on 2/26/21.

import UIKit

@IBDesignable class ERUIShadowLabel : ERUIShapeLabel {
    
    //MARK: - SHADOW
    
    var shadowStyle: ERShadowStyle = ERShadowStyle() {
        didSet { //self.updateShadowAppereance()
            
        }
    }
    
    //@IBInspectable internal var shadowColor : UIColor {
       // get {
            //self.shadowStyle.color
            
       // }
       // set {
            //self.shadowStyle.color = newValue
            
       // }
   // }
    
    @IBInspectable internal var shadowRadius: CGFloat {
        get { self.shadowStyle.radius }
        set { self.shadowStyle.radius = newValue }
    }
    
    @IBInspectable internal var shadowOpacity: Float {
        get { self.shadowStyle.opacity }
        set { self.shadowStyle.opacity = newValue }
    }
    
    @IBInspectable internal override var shadowOffset: CGSize {
        get { self.shadowStyle.offset }
        set { self.shadowStyle.offset = newValue }
    }
}
